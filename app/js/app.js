var language;
var langFlag;

var dict = {

  ge: {
    'question_title': 'Wo ist der Hauptsitz von gomo?',
    'submit': 'Einreichen',
    'facts_title': 'Fakten über Brighton',
    'facts_text': 'Brighton ist ein englischer Badeort. Etwa eine Stunde südlich von London mit dem Zug. Es ist ein beliebtes Ausflugsziel. Der breite Kieselstrand wird von Spielhallen und Regency-Ära-Gebäuden, Brighton Pier in der zentralen Uferpromenade Abschnitt, eröffnet im Jahre 1899 und jetzt hat Fahrten und Lebensmittel-Kioske unterstützt. Die Stadt ist auch bekannt für ihr Nachtleben, Kunstszene, Einkaufen und Festivals.',
    'modal_success': 'Gut gemacht, gomo\'s Hauptsitz ist in Brighton!',
    'modal_fail': 'Falsch! Gomos Hauptsitz ist nicht da! Versuch es noch einmal!'
  },

  en: {
    'question_title': 'Where is gomo\'s head office?',
    'submit': 'Submit',
    'facts_title': 'Facts about Brighton',
    'facts_text': 'Brighton is an English seaside resort town. About an hour south of London by train. It\'s a popular day-trip destination. Its broad shingle beach is backed by amusement arcades and Regency-era buildings, Brighton Pier in the central waterfront section, opened in 1899 and now has rides and food kiosks. The town is also known for its nightlife, arts scene, shopping and festivals.',
    'modal_success': 'Well done, gomo\'s head office is in Brighton!',
    'modal_fail': 'Wrong! gomo\'s head office isn\'t there! Try again!',
  }
}


$(document).ready(function() {

  languageSelect();
  loadAdditionalInfo();
  loadModal();

});


function languageSelect() {
  $("#language-select").on("click", function() {
    language = $("#lang-choice :selected").text();
    $(".language-picker").toggleClass("hide");
    $(".gomo-content").toggleClass("hide");
    loadLanguage(language);
  });
};

function loadLanguage(language) {

  if (language == "English") {
    langFlag = 'en';
  } else if (language == "German") {
    langFlag = 'ge';
  };

  $('[data-trans="question_title"').text(dict[langFlag].question_title);
  $('[data-trans="submit"').text(dict[langFlag].submit);
  $('[data-trans="facts_title"').text(dict[langFlag].facts_title);
  $('[data-trans="facts_text"').text(dict[langFlag].facts_text);
}

function loadAdditionalInfo() {
  $("#answer-select").on("click", function() {
    $(".gomo-facts").removeClass("hide");
  });

}

function loadModal() {

  $("#answer-select").on("click", function() {

    if ($('input[name=location]:checked').val() == "Brighton") {
      $('[data-trans="modal_text"').text(dict[langFlag].modal_success);
    } else {
      $('[data-trans="modal_text"').text(dict[langFlag].modal_fail);
    }

    $("#answer-modal").css('display', 'block');

  });

  $(".modal-close").on("click", function() {
    $("#answer-modal").css('display', 'none');
  });
}
